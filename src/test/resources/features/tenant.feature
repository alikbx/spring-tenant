Feature: Tenant Functionality

Scenario Outline: Change Schema Base On TenantId
	Given the TenantContext is Empty
	When Call Order Controller with <tenantId> In Header
	Then the TenantContext should contain <tenantId>

	Examples:
		| tenantId |
		| first_tanent |
		| default_tenant  |