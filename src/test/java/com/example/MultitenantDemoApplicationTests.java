package com.example;

import com.example.config.TenantContext;
import com.example.domain.Order;
import com.example.domain.OrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class MultitenantDemoApplicationTests {

    @Mock
    private OrderRepository orderRepository;

    @Autowired
    private MockMvc mvc;


    @Before
    public void initOrder() {
        Order order = new Order(new Date(System.currentTimeMillis()));
        orderRepository.save(order);
        Mockito.when(orderRepository.save(any(Order.class))).thenReturn(order);
    }

    @Test
    public void Check_Current_TenantId() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-TenantID", "test_tanent");
        mvc.perform(post("/orders").headers(httpHeaders)).andExpect(status().isOk());
        assertEquals(TenantContext.getCurrentTenant(), "test_tanent");
    }
}
