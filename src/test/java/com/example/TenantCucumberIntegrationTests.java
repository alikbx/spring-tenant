package com.example;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TenantCucumberIntegrationTests {
    private final Logger log = LoggerFactory.getLogger(TenantCucumberIntegrationTests.class);

    private final String SERVER_URL = "http://localhost";
    private final String THINGS_ENDPOINT = "/orders";

    private RestTemplate restTemplate;

    public static String currentTanentForTest = "";


    @LocalServerPort
    protected int port;

    public TenantCucumberIntegrationTests() {
        this.restTemplate = new RestTemplate();
    }


    private String orderEndpoint() {
        return SERVER_URL + ":" + port + THINGS_ENDPOINT;
    }


    HttpStatus requestWithCustomTenantId(String tenantId) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-TenantID", tenantId);
        String requestJson = "{}";

        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);


        ResponseEntity<String> response = restTemplate
                .exchange(orderEndpoint(), HttpMethod.POST, entity, String.class);
        currentTanentForTest = response.getBody();
        return response.getStatusCode();
    }

    Object getCurrentTenantId() {
        return currentTanentForTest;
    }

    void clean() {
        currentTanentForTest = null;
    }


}
