package com.example;

import com.example.config.TenantContext;
import cucumber.api.java8.En;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

public class TenantCucmberStepDefinitions extends TenantCucumberIntegrationTests implements En {
    private final Logger log = LoggerFactory.getLogger(TenantCucmberStepDefinitions.class);

    public TenantCucmberStepDefinitions() {

        Given("the TenantContext is Empty", () -> {
            clean();
            Assert.assertNull(TenantContext.getCurrentTenant());
        });

        When("Call Order Controller with {word} In Header", (String tenantId) -> {
            Assert.assertEquals(requestWithCustomTenantId(tenantId), HttpStatus.OK);
        });

        Then("the TenantContext should contain {word}", (String tenantId) -> {
            log.error(getCurrentTenantId().toString());
            Assert.assertEquals(getCurrentTenantId().toString(), tenantId);
        });


    }

}
