package com.example.controllers;

import com.example.config.TenantContext;
import com.example.domain.Order;
import com.example.domain.OrderRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;

@Controller
@RestController
@Api(value = "Order Management System", description = "this is dummy Controller to Test Multi Tenant")
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;

    @ApiOperation(value = "View a Tenant-Id that sent in Header Request", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Tenant-Id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(path = "/orders", method = RequestMethod.POST)
    public ResponseEntity<String> createSampleOrder() {

        //TenantContext.setCurrentTenant(tenantName);

        Order newOrder = new Order(new Date(System.currentTimeMillis()));
        orderRepository.save(newOrder);
        return ResponseEntity.ok().body(TenantContext.getCurrentTenant().toString());
    }


}
