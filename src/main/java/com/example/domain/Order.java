package com.example.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "orders")
@Getter
@Setter
@ApiModel(description = "All details about dummy Order Model. ")
public class Order {

    public Order() {
    }

    public Order(Date date)
    {
        this.date = date;
    }

    @Id
    @Column(nullable = false, name = "id")
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated order ID")
    private Integer id;

    @Column(nullable = false, name = "date")
    @ApiModelProperty(notes = "The order Date")
    private Date date;

    

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (id != null ? !id.equals(order.id) : order.id != null) return false;
        return date != null ? date.equals(order.date) : order.date == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
